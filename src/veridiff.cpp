
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <tuple>
#include <fstream>
#include <random>

#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/AST/Type.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Lex/Lexer.h"
#include "clang/Parse/ParseAST.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/raw_ostream.h"

#include "clara.hpp"
#include "json.hpp"

// for convenience
using json = nlohmann::json;
using namespace clara;

using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace clang::ast_matchers;


// Part below taken from LLVM/Clang
//===--- Transforms.cpp - Transformations to ARC mode ---------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//


/// \brief \arg Loc is the end of a statement range. This returns the location
/// of the semicolon following the statement.
/// If no semicolon is found or the location is inside a macro, the returned
/// source location will be invalid.
SourceLocation findSemiAfterLocation(SourceLocation loc,
	ASTContext &Ctx,
	bool IsDecl) {
	SourceManager &SM = Ctx.getSourceManager();
	if (loc.isMacroID()) {
		if (!Lexer::isAtEndOfMacroExpansion(loc, SM, Ctx.getLangOpts(), &loc))
			return SourceLocation();
	}
	loc = Lexer::getLocForEndOfToken(loc, /*Offset=*/0, SM, Ctx.getLangOpts());

	// Break down the source location.
	std::pair<FileID, unsigned> locInfo = SM.getDecomposedLoc(loc);

	// Try to load the file buffer.
	bool invalidTemp = false;
	StringRef file = SM.getBufferData(locInfo.first, &invalidTemp);
	if (invalidTemp)
		return SourceLocation();

	const char *tokenBegin = file.data() + locInfo.second;

	// Lex from the start of the given location.
	Lexer lexer(SM.getLocForStartOfFile(locInfo.first),
		Ctx.getLangOpts(),
		file.begin(), tokenBegin, file.end());
	Token tok;
	lexer.LexFromRawLexer(tok);
	if (tok.isNot(tok::semi)) {
		if (!IsDecl)
			return SourceLocation();
		// Declaration may be followed with other tokens; such as an __attribute,
		// before ending with a semicolon.
		return findSemiAfterLocation(tok.getLocation(), Ctx, /*IsDecl*/true);
	}

	return tok.getLocation();
}


/// \brief 'Loc' is the end of a statement range. This returns the location
/// immediately after the semicolon following the statement.
/// If no semicolon is found or the location is inside a macro, the returned
/// source location will be invalid.
SourceLocation findLocationAfterSemi(SourceLocation loc,
	ASTContext &Ctx, bool IsDecl) {
	SourceLocation SemiLoc = findSemiAfterLocation(loc, Ctx, IsDecl);
	if (SemiLoc.isInvalid())
		return SourceLocation();
	return SemiLoc.getLocWithOffset(1);
}



//================================



static llvm::cl::OptionCategory ToolingSampleCategory("Tooling Sample");

enum class PositionType { NORMAL, CONDITION_INIT_OR_STEP, SINGLE_STATEMENT, INITIALIZATION, SINGLE_INITIALIZATION };
enum class AccessType { READ, WRITE};
struct Access {
	QualType type;
	std::string variable;
};

struct Entry {
	PositionType type;
	const Stmt* statement;
	const Stmt* singleStatementParent = nullptr;
	std::vector<Access> reads;
	std::vector<Access> writes;
};


class StmtHandler : public MatchFinder::MatchCallback {
public:
	StmtHandler(Rewriter &Rewrite, std::vector<Entry>& entries) : Rewrite(Rewrite), entries(entries) {
	}

	virtual void run(const MatchFinder::MatchResult &Result) {
		if (const IfStmt *IfS = Result.Nodes.getNodeAs<clang::IfStmt>("statement")) {
			const Stmt *Cond = IfS->getCond();
			entries.emplace_back();
			entries.back().type = PositionType::CONDITION_INIT_OR_STEP;
			entries.back().statement = Cond;
			return;
		}
		if (const ForStmt *ForS = Result.Nodes.getNodeAs<clang::ForStmt>("statement")) {
			const Stmt *Cond = ForS->getCond();
			entries.emplace_back();
			entries.back().type = PositionType::CONDITION_INIT_OR_STEP;
			entries.back().statement = Cond;
			const Stmt *Init = ForS->getInit();
			entries.emplace_back();
			entries.back().type = PositionType::CONDITION_INIT_OR_STEP;
			entries.back().statement = Init;
			const Stmt *Inc = ForS->getInc();
			entries.emplace_back();
			entries.back().type = PositionType::CONDITION_INIT_OR_STEP;
			entries.back().statement = Inc;
			return;
		}
		if (const WhileStmt *WhileS = Result.Nodes.getNodeAs<clang::WhileStmt>("statement")) {
			const Stmt *Cond = WhileS->getCond();
			entries.emplace_back();
			entries.back().type = PositionType::CONDITION_INIT_OR_STEP;
			entries.back().statement = Cond;
			return;
		}
		if (const DoStmt *DoS = Result.Nodes.getNodeAs<clang::DoStmt>("statement")) {
			const Stmt *Cond = DoS->getCond();
			entries.emplace_back();
			entries.back().type = PositionType::CONDITION_INIT_OR_STEP;
			entries.back().statement = Cond;
			return;
		}

		if (const LabelStmt *LabelS = Result.Nodes.getNodeAs<clang::LabelStmt>("statement")) {
			const Stmt *SubStmt = LabelS->getSubStmt();
			entries.emplace_back();
			entries.back().type = PositionType::NORMAL;
			entries.back().statement = SubStmt;
			return;
		}

		entries.emplace_back();
		entries.back().type = PositionType::NORMAL;
		entries.back().statement = Result.Nodes.getNodeAs<clang::Stmt>("statement");
	}

private:
	Rewriter &Rewrite;
	std::vector<Entry> &entries;
};


class SingleStmtHandler : public MatchFinder::MatchCallback {
public:
	SingleStmtHandler(Rewriter &Rewrite, std::vector<Entry>& entries) : Rewrite(Rewrite), entries(entries) {
	}

	virtual void run(const MatchFinder::MatchResult &Result) {
		if (const NullStmt *NullS = Result.Nodes.getNodeAs<clang::NullStmt>("directly")) {
			return;
		}
		entries.emplace_back();
		entries.back().type = PositionType::SINGLE_STATEMENT;
		entries.back().statement = Result.Nodes.getNodeAs<clang::Stmt>("directly");
		entries.back().singleStatementParent = Result.Nodes.getNodeAs<clang::Stmt>("parent");
	}

private:
	Rewriter &Rewrite;
	std::vector<Entry> &entries;
};

class DeclStmtHandler : public MatchFinder::MatchCallback {
public:
	DeclStmtHandler(Rewriter &Rewrite, std::vector<Entry>& entries) : Rewrite(Rewrite), entries(entries) {
	}

	virtual void run(const MatchFinder::MatchResult &Result) {
		if (Result.Nodes.getNodeAs<clang::CompoundStmt>("parent")) {
			entries.emplace_back();
			entries.back().type = PositionType::INITIALIZATION;
			entries.back().statement = Result.Nodes.getNodeAs<clang::DeclStmt>("declStmt");
		}
		else {
			entries.emplace_back();
			entries.back().type = PositionType::SINGLE_INITIALIZATION;
			entries.back().statement = Result.Nodes.getNodeAs<clang::DeclStmt>("declStmt");
		}
	}

private:
	Rewriter &Rewrite;
	std::vector<Entry> &entries;
};


class AssertHandler : public MatchFinder::MatchCallback {
public:
	AssertHandler(Rewriter &Rewrite) : Rewrite(Rewrite) {
	}

	virtual void run(const MatchFinder::MatchResult &Result) {
		if (Result.Nodes.getNodeAs<clang::FunctionDecl>("function")) {
			auto fundecl = Result.Nodes.getNodeAs<clang::FunctionDecl>("function");
			auto startPosition = fundecl->getLocStart();
			Rewrite.InsertTextBefore(startPosition, "void DUMMY__VERIFIER_assert(int cond) {}\n");
		}
		else {
			auto funcall = Result.Nodes.getNodeAs<clang::CallExpr>("call");
			auto startPosition = funcall->getLocStart();
			Rewrite.InsertTextBefore(startPosition, "DUMMY");
		}
	}

private:
	Rewriter &Rewrite;
};


// Implementation of the ASTConsumer interface for reading an AST produced
// by the Clang parser.
class MyASTConsumer : public ASTConsumer {
public:
  MyASTConsumer(Rewriter &R, std::vector<Entry>& entries) : statementHandler(R,entries), singleStatementHandler(R, entries), declStatementHandler(R, entries), assertHandler(R){
	  Matcher.addMatcher(
		  stmt(allOf(unless(anyOf(hasAncestor(functionDecl(hasName("__VERIFIER_assert"))), compoundStmt(), declStmt(), nullStmt())), hasParent(compoundStmt()))).bind("statement")
		  , &statementHandler);
	  Matcher.addMatcher(
		  functionDecl(hasName("__VERIFIER_assert")).bind("function")
		  , &assertHandler);
	  Matcher.addMatcher(
		  callExpr(callee(functionDecl(hasName("__VERIFIER_assert")))).bind("call")
		  , &assertHandler);

	  Matcher.addMatcher(
		  forStmt(allOf(
			  hasBody(stmt(unless(compoundStmt())).bind("directly")), 
			  unless(hasAncestor(functionDecl(hasName("__VERIFIER_assert")))))).bind("parent")
		  , &singleStatementHandler);
	  Matcher.addMatcher(
		  whileStmt(allOf(hasBody(stmt(unless(compoundStmt())).bind("directly")), unless(hasAncestor(functionDecl(hasName("__VERIFIER_assert")))))).bind("parent")
		  , &singleStatementHandler);
	  Matcher.addMatcher(
		  doStmt(allOf(hasBody(stmt(unless(compoundStmt())).bind("directly")), unless(hasAncestor(functionDecl(hasName("__VERIFIER_assert")))))).bind("parent")
		  , &singleStatementHandler);
	  Matcher.addMatcher(
		  ifStmt(allOf(hasThen(stmt(unless(compoundStmt())).bind("directly")), unless(hasAncestor(functionDecl(hasName("__VERIFIER_assert")))))).bind("parent")
		  , &singleStatementHandler);
	  Matcher.addMatcher(
		  ifStmt(allOf(hasElse(stmt(unless(compoundStmt())).bind("directly")), unless(hasAncestor(functionDecl(hasName("__VERIFIER_assert")))))).bind("parent")
		  , &singleStatementHandler);

	  Matcher.addMatcher(declStmt(allOf(hasParent(stmt().bind("parent")),has(varDecl(allOf(unless(parmVarDecl()), hasInitializer(expr())))))).bind("declStmt"), &declStatementHandler);
  }
  void HandleTranslationUnit(ASTContext &Context) override {
	  // Run the matchers when we have the whole TU parsed.
	  Matcher.matchAST(Context);
  }


private:
	MatchFinder Matcher;
	StmtHandler statementHandler;
	SingleStmtHandler singleStatementHandler;
	DeclStmtHandler declStatementHandler;
	AssertHandler assertHandler;
};


bool isNotModifying(const Expr* expr) {
	if (isa<DeclRefExpr>(expr)) {
		auto declRefExpr = cast<DeclRefExpr>(expr);
		auto usedDecl = declRefExpr->getFoundDecl();
		if (isa<VarDecl>(usedDecl)) {
			return true;
		}
	}

	if (isa<ImplicitCastExpr>(expr)) {
		auto implicitCast = cast<ImplicitCastExpr>(expr);
		auto subexpr = implicitCast->getSubExpr();
		return isNotModifying(subexpr);
	}

	if (isa<IntegerLiteral>(expr) || isa<CharacterLiteral>(expr) || isa<StringLiteral>(expr) || isa<FloatingLiteral>(expr)) {
		return true;
	}

	std::cerr << "isNotModifying: UNKNOWN EXPRESSION" << std::endl;
	expr->dumpColor();
	return false;
}

SourceLocation end_of_the_end(SourceLocation const & start_of_end, SourceManager & sm) {
	LangOptions lopt;
	return Lexer::getLocForEndOfToken(start_of_end, 0, sm, lopt);
}

void handleRead(Entry& entry, const Expr* expr, SourceManager & sm);

void handleWrite(Entry& entry, const Expr* expr, SourceManager & sm) {
	if (isa<DeclRefExpr>(expr)) {
		auto declRefExpr = cast<DeclRefExpr>(expr);
		auto usedDecl = declRefExpr->getFoundDecl();
		if (isa<VarDecl>(usedDecl)) {
			auto varDecl = cast<VarDecl>(usedDecl);
			Access access = { varDecl->getType(),varDecl->getName() };
			entry.writes.push_back(access);
			return;
		}

	}
	if (isa<ArraySubscriptExpr>(expr)) {
		auto arraySubscriptExpr = cast<ArraySubscriptExpr>(expr);
		auto arrayExpression = arraySubscriptExpr->getBase();
		handleRead(entry, arrayExpression, sm);
		auto indexExpression = arraySubscriptExpr->getIdx();
		handleRead(entry, indexExpression, sm);
		if (isNotModifying(arraySubscriptExpr->getBase()) && isNotModifying(arraySubscriptExpr->getIdx())) {
			SourceRange decl_range(expr->getSourceRange());
			SourceLocation decl_begin(decl_range.getBegin());
			SourceLocation decl_start_end(decl_range.getEnd());
			SourceLocation decl_end_end(end_of_the_end(decl_start_end, sm)); 
			const char * buff_begin(sm.getCharacterData(decl_begin));
			const char * buff_end(sm.getCharacterData(decl_end_end));
			std::string const arrayString(buff_begin, buff_end);
			Access access = { expr->getType(),arrayString };
			entry.writes.push_back(access);
		} else{
			std::cerr << "Write: Array position write was not deemed to not modify" << std::endl;
		}

		return;
	}


	if (isa<UnaryOperator>(expr)) {
		auto unaryOp = cast<UnaryOperator>(expr);
		switch (unaryOp->getOpcode()) {
		case UnaryOperatorKind::UO_Deref:
		{
			auto subexpr = unaryOp->getSubExpr();
			if (isNotModifying(subexpr)) {
				SourceRange decl_range(expr->getSourceRange());
				SourceLocation decl_begin(decl_range.getBegin());
				SourceLocation decl_start_end(decl_range.getEnd());
				SourceLocation decl_end_end(end_of_the_end(decl_start_end, sm));
				const char * buff_begin(sm.getCharacterData(decl_begin));
				const char * buff_end(sm.getCharacterData(decl_end_end));
				std::string const arrayString(buff_begin, buff_end);
				Access access = { expr->getType(),arrayString };
				entry.writes.push_back(access);
			}
			handleRead(entry, subexpr, sm); // treat it as a read
			return;
		}
		break;
		default:
			break;
		}
	}

	std::cerr << "WRITE: UNKNOWN EXPRESSION" << std::endl;
	expr->dumpColor();
}

void handleRead(Entry& entry, const Expr* expr, SourceManager & sm) {
	if (isa<IntegerLiteral>(expr) || isa<CharacterLiteral>(expr) || isa<StringLiteral>(expr) || isa<FloatingLiteral>(expr)) {
		return;
	}
	if (isa<InitListExpr>(expr)) {
		auto initListExpr = cast<InitListExpr>(expr);
		std::for_each(initListExpr->child_begin(), initListExpr->child_end(), [&entry,&sm](const Stmt* stmt) {
			const Expr* expr = cast<Expr>(stmt);
			handleRead(entry, expr, sm);
		});
		return;
	}

	if (isa<DeclRefExpr>(expr)) {
		auto declRefExpr = cast<DeclRefExpr>(expr);
		auto usedDecl = declRefExpr->getFoundDecl();
		if (isa<VarDecl>(usedDecl)) {
			auto varDecl = cast<VarDecl>(usedDecl);
			Access access = { varDecl->getType(),varDecl->getName() };
			entry.reads.push_back(access);
			return;
		}
		
	}

	if (isa<UnaryOperator>(expr)) {
		auto unaryOp =cast<UnaryOperator>(expr);
		switch (unaryOp->getOpcode()) {
		case UnaryOperatorKind::UO_AddrOf:
		case UnaryOperatorKind::UO_Deref:
		case UnaryOperatorKind::UO_Minus:
		case UnaryOperatorKind::UO_Plus:
		case UnaryOperatorKind::UO_Not:
		case UnaryOperatorKind::UO_LNot :
			handleRead(entry, unaryOp->getSubExpr(), sm); // ASSUME that the operators are not overloaded
			return;
		case UnaryOperatorKind::UO_PreInc:
		case UnaryOperatorKind::UO_PostInc:
		case UnaryOperatorKind::UO_PostDec:
		case UnaryOperatorKind::UO_PreDec:
		{
			auto subexpr = unaryOp->getSubExpr();
			handleWrite(entry, subexpr, sm); // treat it as a write
			handleRead(entry, subexpr, sm); // treat it as a read
			return;
		}
			break;
		default:
			break;
		}
	}

	if (isa<BinaryOperator>(expr)) {
		auto binOp = cast<BinaryOperator>(expr);
		switch (binOp->getOpcode()) {
		case BinaryOperatorKind::BO_Add:
		case BinaryOperatorKind::BO_And:
		case BinaryOperatorKind::BO_Comma:
		case BinaryOperatorKind::BO_Div:
		case BinaryOperatorKind::BO_EQ:
		case BinaryOperatorKind::BO_GT:
		case BinaryOperatorKind::BO_GE:
		case BinaryOperatorKind::BO_LT:
		case BinaryOperatorKind::BO_LE:
		case BinaryOperatorKind::BO_LAnd:
		case BinaryOperatorKind::BO_LOr:
		case BinaryOperatorKind::BO_Mul:
		case BinaryOperatorKind::BO_NE:
		case BinaryOperatorKind::BO_Or:
		case BinaryOperatorKind::BO_Rem:
		case BinaryOperatorKind::BO_Xor:
		{
			handleRead(entry, binOp->getLHS(), sm);
			handleRead(entry, binOp->getRHS(), sm);
			return;
		}break;
		default: break;
		}

	}

	if (isa<ImplicitCastExpr>(expr)) {
		auto implicitCast = cast<ImplicitCastExpr>(expr);
		auto subexpr = implicitCast->getSubExpr();
		handleRead(entry, subexpr, sm);
		return;
	}
	if (isa<ParenExpr>(expr)) {
		auto parenExpr = cast<ParenExpr>(expr);
		auto subexpr = parenExpr->getSubExpr();
		handleRead(entry, subexpr, sm);
		return;
	}

	if (isa<CallExpr>(expr)) {
		auto callExpr = cast<CallExpr>(expr);
		auto args = callExpr->getArgs();
		const auto numArgs = callExpr->getNumArgs();
		for (int i = 0; i < numArgs; ++i) {
			handleRead(entry, args[i], sm);
		}
		return;
	}

	if (isa<ArraySubscriptExpr>(expr)) {
		auto arraySubscriptExpr = cast<ArraySubscriptExpr>(expr);
		auto arrayExpression = arraySubscriptExpr->getBase();
		handleRead(entry, arrayExpression, sm);
		auto indexExpression = arraySubscriptExpr->getIdx();
		handleRead(entry, indexExpression, sm);
		if (isNotModifying(arraySubscriptExpr->getBase()) && isNotModifying(arraySubscriptExpr->getIdx())) {
			SourceRange decl_range(expr->getSourceRange());
			SourceLocation decl_begin(decl_range.getBegin());
			SourceLocation decl_start_end(decl_range.getEnd());
			SourceLocation decl_end_end(end_of_the_end(decl_start_end, sm)); const char * buff_begin(sm.getCharacterData(decl_begin));
			const char * buff_end(sm.getCharacterData(decl_end_end));
			std::string const arrayString(buff_begin, buff_end);
			Access access = { expr->getType(),arrayString };
			entry.reads.push_back(access);
		}
		else {
			std::cerr << "Read: Array position read was not deemed to not modify" << std::endl;
		}
		return;
	}

	std::cerr << "Read: UNKNOWN EXPRESSION" << std::endl;
	expr->dumpColor();
}

void handleStatement(Entry& entry, SourceManager & sm) {
	if (isa<BinaryOperator>(entry.statement)) {
		auto binOp = cast<BinaryOperator>(entry.statement);
		if (BinaryOperatorKind::BO_Assign == binOp->getOpcode()) {
			handleWrite(entry, binOp->getLHS(), sm);
		}
		else {
			handleRead(entry, binOp->getLHS(), sm);
		}
		handleRead(entry, binOp->getRHS(), sm);
		return;
	}
	if (isa<UnaryOperator>(entry.statement)) {
		auto unaryOp = cast<UnaryOperator>(entry.statement);
		handleRead(entry, unaryOp, sm);
		return;
	}
	if (isa<ReturnStmt>(entry.statement)) {
		auto retStmt = cast<ReturnStmt>(entry.statement);
		handleRead(entry, retStmt->getRetValue(), sm);
		return;
	}
	if (isa<CallExpr>(entry.statement)) {
		auto callExpr = cast<CallExpr>(entry.statement);
		handleRead(entry, callExpr, sm);
		return;
	}
	if (isa<IntegerLiteral>(entry.statement) || isa<CharacterLiteral>(entry.statement) || isa<StringLiteral>(entry.statement)) {
		return;
	}

	if (isa<ImplicitCastExpr>(entry.statement)) {
		auto implicitCast = cast<ImplicitCastExpr>(entry.statement);
		handleRead(entry, implicitCast, sm);
		return;
	}

	FullSourceLoc fullLoc(entry.statement->getLocStart(), sm);
	const unsigned int lineNum = fullLoc.getSpellingLineNumber();
	std::cerr << "UNKNOWN STATEMENT at line "<< lineNum << std::endl;
	entry.statement->dumpColor();
}

void handleDeclaration(Entry& entry, SourceManager & sm) {
	const DeclStmt* declStmt = cast<DeclStmt>(entry.statement);
	if (declStmt->isSingleDecl()) {
		const VarDecl* varDecl = cast<VarDecl>(declStmt->getSingleDecl());
		auto type = varDecl->getType();
		auto init = varDecl->getInit();
		if (type.isConstQualified()) {
			//TODO handleRead(...) on init part
			std::cerr << "const definition will not be split\n";
			declStmt->dumpColor();
		}
		else {
			if (isa<InitListExpr>(init)) {
				//TODO handleRead(...) on init part
				std::cerr << "definition with InitListExpr will not be split\n";
				declStmt->dumpColor();
			}
			else {
				Access access = { type,varDecl->getQualifiedNameAsString() };
				entry.writes.push_back(access);
			}
		}
		handleRead(entry, init, sm);

	}
	else {
		//TODO handleRead(...)
		std::cerr << "combined definition not supported\n";
		declStmt->dumpColor();
	}
}

void getReadsAndWrites(std::vector<Entry>& entries, SourceManager & sm) {
	for (auto&& entry : entries) {
		switch (entry.type) {
		case PositionType::NORMAL: 
		case PositionType::CONDITION_INIT_OR_STEP: 
		case PositionType::SINGLE_STATEMENT: {
			handleStatement(entry, sm);
		} break;
		case PositionType::INITIALIZATION: 
		case PositionType::SINGLE_INITIALIZATION: {
			handleDeclaration(entry, sm);
		} break;
		}
	}

}
void removeUnusedEntries(std::vector<Entry>& entries) {
	entries.erase(std::remove_if(begin(entries), end(entries), [](Entry& entry) {return entry.reads.empty() && entry.writes.empty(); }), end(entries));
}

void parseInstrumentationFromJSON(std::string &InstrumentationJSON, std::vector<Entry> &entries, std::vector<std::tuple<int, std::string>> &assertsToEnter, int &numReads);

void instrument(std::string &SrcFileName, std::string &JSONOutputFile, std::string &InstrumentationJSON, std::string &InstrumentedFileName);

void instrument(std::string &SrcFileName, std::string &JSONOutputFile, std::string &InstrumentationJSON, std::string &InstrumentedFileName)
{
	// CompilerInstance will hold the instance of the Clang compiler for us,
	// managing the various objects needed to run the compiler.
	CompilerInstance TheCompInst;
	TheCompInst.createDiagnostics();

	LangOptions &lo = TheCompInst.getLangOpts();
	lo.CPlusPlus = 0;

	// Initialize target info with the default triple for our platform.
	auto TO = std::make_shared<TargetOptions>();
	TO->Triple = llvm::sys::getDefaultTargetTriple();
	TargetInfo *TI =
		TargetInfo::CreateTargetInfo(TheCompInst.getDiagnostics(), TO);
	TheCompInst.setTarget(TI);

	TheCompInst.createFileManager();
	FileManager &FileMgr = TheCompInst.getFileManager();
	TheCompInst.createSourceManager(FileMgr);
	SourceManager &SourceMgr = TheCompInst.getSourceManager();
	TheCompInst.createPreprocessor(TU_Module);
	TheCompInst.createASTContext();

	// A Rewriter helps us manage the code rewriting task.
	Rewriter TheRewriter;
	TheRewriter.setSourceMgr(SourceMgr, TheCompInst.getLangOpts());

	// Set the main file handled by the source manager to the input file.
	const FileEntry *FileIn = FileMgr.getFile(SrcFileName);
	SourceMgr.setMainFileID(
		SourceMgr.createFileID(FileIn, SourceLocation(), SrcMgr::C_User));
	TheCompInst.getDiagnosticClient().BeginSourceFile(
		TheCompInst.getLangOpts(), &TheCompInst.getPreprocessor());


	std::vector<Entry> entries{};

	// Create an AST consumer instance which is going to get called by
	// ParseAST.
	MyASTConsumer TheConsumer(TheRewriter, entries);

	// Parse the file to AST, registering our consumer as the AST consumer.
	ParseAST(TheCompInst.getPreprocessor(), &TheConsumer,
		TheCompInst.getASTContext());



	getReadsAndWrites(entries, SourceMgr);
	removeUnusedEntries(entries);

	std::cerr.flush();
	std::cout << "\n\n";
	std::cout.flush();

	json readAndWriteJSON = json::array();

	for (auto&& entry : entries) {
		json statementJSON = json::object();
		switch (entry.type)
		{
		case PositionType::CONDITION_INIT_OR_STEP:
		{
			statementJSON["type"] = "CONDITION_INIT_OR_STEP";
			//std::cout << "CONDITION_INIT_OR_STEP";
			break;
		}
		case PositionType::SINGLE_STATEMENT:
		{
			statementJSON["type"] = "SINGLE_STATEMENT";
			//std::cout << "SINGLE_STATEMENT";
			break;
		}
		case PositionType::NORMAL:
		{
			statementJSON["type"] = "NORMAL";
			//std::cout << "NORMAL";
			break;
		}
		case PositionType::SINGLE_INITIALIZATION:
		{
			statementJSON["type"] = "SINGLE_INITIALIZATION";
			//std::cout << "SINGLE_INITIALIZATION";
			break;
		}
		case PositionType::INITIALIZATION:
		{
			statementJSON["type"] = "INITIALIZATION";
			//std::cout << "INITIALIZATION";
			break;
		}
		}

		{
			FullSourceLoc fullLoc(entry.statement->getLocStart(), SourceMgr);
			const unsigned int lineNum = fullLoc.getSpellingLineNumber();
			statementJSON["lineNumber"] = lineNum;
		}

		statementJSON["reads"] = json::array();
		statementJSON["writes"] = json::array();
		for (auto&& access : entry.reads) {
			json accessJSON = json::object();
			accessJSON["accessedEntity"] = access.variable;
			accessJSON["type"] = access.type.getAsString();
			statementJSON["reads"].push_back(accessJSON);
		}
		for (auto&& access : entry.writes) {
			json accessJSON = json::object();
			accessJSON["accessedEntity"] = access.variable;
			accessJSON["type"] = access.type.getAsString();
			statementJSON["writes"].push_back(accessJSON);
		}
		readAndWriteJSON.push_back(statementJSON);
	}


	int numReads = 0;
	int numWrites = 0;

	for (auto&& entry : entries) {
		numReads += entry.reads.size();
		numWrites += entry.writes.size();
	}
	json outputJSON = json::object();
	outputJSON["statements"] = readAndWriteJSON;
	outputJSON["numberOfReads"] = numReads;
	outputJSON["numberOfWrites"] = numWrites;
	if (JSONOutputFile.empty()) {
	}
	else {
		std::ofstream o(JSONOutputFile);
		o << std::setw(2) << outputJSON << std::endl;
		o.close();
	}
	// now the instrumentation part

	std::vector<std::tuple<int, std::string>> assertsToEnter{};

	
	if (InstrumentationJSON.empty())
	{
		//dump all reads and writes
		int readCounter = 0;
		int writeCounter = 0;

		for (auto && entry : entries)
		{
			switch (entry.type)
			{
			case PositionType::NORMAL: {
				auto iter = entry.reads.rbegin();
				auto endIter = entry.reads.rend();
				int localOffset = entry.reads.size() - 1;
				for (; iter != endIter; ++iter) {
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "\n");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), std::to_string(readCounter + localOffset));
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), ") 0x0); // read");
					if (isa<ArrayType>(iter->type.getTypePtr())) {
						const ArrayType* arrayType = cast<ArrayType>(iter->type.getTypePtr());
						std::string assertionString = arrayType->getElementType().getAsString() + " *";
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					else {

						std::string assertionString = iter->type.getAsString();
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), " != (");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), iter->variable);
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "__VERIFIER_assert(");
					--localOffset;
				}
				iter = entry.writes.rbegin();
				endIter = entry.writes.rend();
				localOffset = entry.writes.size() - 1;
				for (; iter != endIter; ++iter) {
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "\n");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), std::to_string(writeCounter + localOffset));
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), ") 0x0); // write");
					if (isa<ArrayType>(iter->type.getTypePtr())) {
						const ArrayType* arrayType = cast<ArrayType>(iter->type.getTypePtr());
						std::string assertionString = arrayType->getElementType().getAsString() + " *";
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					else {

						std::string assertionString = iter->type.getAsString();
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), " != (");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), iter->variable);
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "__VERIFIER_assert(");
					--localOffset;
				}

				//TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "\n");
				readCounter += entry.reads.size();
				writeCounter += entry.writes.size();

			} break;
			case PositionType::CONDITION_INIT_OR_STEP: {
				auto iter = entry.reads.rbegin();
				auto endIter = entry.reads.rend();
				int localOffset = entry.reads.size() - 1;
				for (; iter != endIter; ++iter) {
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "\n");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), std::to_string(readCounter + localOffset));
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), ") 0x0), // read");
					if (isa<ArrayType>(iter->type.getTypePtr())) {
						const ArrayType* arrayType = cast<ArrayType>(iter->type.getTypePtr());
						std::string assertionString = arrayType->getElementType().getAsString() + " *";
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					else {

						std::string assertionString = iter->type.getAsString();
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), " != (");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), iter->variable);
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "__VERIFIER_assert(");
					--localOffset;
				}

				iter = entry.writes.rbegin();
				endIter = entry.writes.rend();
				localOffset = entry.writes.size() - 1;
				for (; iter != endIter; ++iter) {
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "\n");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), std::to_string(writeCounter + localOffset));
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), ") 0x0), // write");
					if (isa<ArrayType>(iter->type.getTypePtr())) {
						const ArrayType* arrayType = cast<ArrayType>(iter->type.getTypePtr());
						std::string assertionString = arrayType->getElementType().getAsString() + " *";
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					else {

						std::string assertionString = iter->type.getAsString();
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), " != (");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), iter->variable);
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "__VERIFIER_assert(");
					--localOffset;
				}

				TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "\n");
				readCounter += entry.reads.size();
				writeCounter += entry.writes.size();
			} break;
			case PositionType::SINGLE_STATEMENT: {
				//TheRewriter.InsertTextBefore(statement.statement->getLocStart(), ");");
				//TheRewriter.InsertTextBefore(statement.statement->getLocStart(), std::get<1>(assertion));
				//TheRewriter.InsertTextBefore(statement.statement->getLocStart(), "{__VERIFIER_assert(");
				SourceLocation SemiLoc = findLocationAfterSemi(entry.statement->getLocEnd(), TheCompInst.getASTContext(), false);
				TheRewriter.InsertTextBefore(SemiLoc, "}");


				auto iter = entry.reads.rbegin();
				auto endIter = entry.reads.rend();
				int localOffset = entry.reads.size() - 1;
				for (; iter != endIter; ++iter) {
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "\n");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), std::to_string(readCounter + localOffset));
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), ") 0x0); // read");
					if (isa<ArrayType>(iter->type.getTypePtr())) {
						const ArrayType* arrayType = cast<ArrayType>(iter->type.getTypePtr());
						std::string assertionString = arrayType->getElementType().getAsString() + " *";
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					else {

						std::string assertionString = iter->type.getAsString();
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), " != (");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), iter->variable);
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "__VERIFIER_assert(");
					--localOffset;
				}

				iter = entry.writes.rbegin();
				endIter = entry.writes.rend();
				localOffset = entry.writes.size() - 1;
				for (; iter != endIter; ++iter) {
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "\n");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), std::to_string(writeCounter + localOffset));
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), ") 0x0); // write");
					if (isa<ArrayType>(iter->type.getTypePtr())) {
						const ArrayType* arrayType = cast<ArrayType>(iter->type.getTypePtr());
						std::string assertionString = arrayType->getElementType().getAsString() + " *";
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					else {

						std::string assertionString = iter->type.getAsString();
						TheRewriter.InsertTextBefore(entry.statement->getLocStart(), assertionString);
					}
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), " != (");
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), iter->variable);
					TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "__VERIFIER_assert(");
					--localOffset;
				}

				TheRewriter.InsertTextBefore(entry.statement->getLocStart(), "{\n");
				readCounter += entry.reads.size();
				writeCounter += entry.writes.size();
			} break;
			case PositionType::INITIALIZATION: {
				const DeclStmt* declStmt = cast<DeclStmt>(entry.statement);
				if (declStmt->isSingleDecl()) {
					const VarDecl* varDecl = cast<VarDecl>(declStmt->getSingleDecl());
					if (varDecl->getInitStyle() == clang::VarDecl::InitializationStyle::CInit) {
						auto init = varDecl->getInit();
						auto startPosition = varDecl->getLocStart();
						while (*SourceMgr.getCharacterData(startPosition) != '=') {
							startPosition = startPosition.getLocWithOffset(1);
						}
						TheRewriter.InsertTextBefore(startPosition, varDecl->getQualifiedNameAsString());
						//TheRewriter.InsertTextBefore(startPosition, ");");
						//TheRewriter.InsertTextBefore(startPosition, std::get<1>(assertion));
						//TheRewriter.InsertTextBefore(startPosition, ";__VERIFIER_assert(");
						auto iter = entry.reads.rbegin();
						auto endIter = entry.reads.rend();
						int localOffset = entry.reads.size() - 1;
						for (; iter != endIter; ++iter) {
							TheRewriter.InsertTextBefore(startPosition, "\n");
							TheRewriter.InsertTextBefore(startPosition, std::to_string(readCounter + localOffset));
							TheRewriter.InsertTextBefore(startPosition, ") 0x0); // read");
							if (isa<ArrayType>(iter->type.getTypePtr())) {
								const ArrayType* arrayType = cast<ArrayType>(iter->type.getTypePtr());
								std::string assertionString = arrayType->getElementType().getAsString() + "*";
								TheRewriter.InsertTextBefore(startPosition, assertionString);
							}
							else {

								std::string assertionString = iter->type.getAsString();
								TheRewriter.InsertTextBefore(startPosition, assertionString);
							}
							TheRewriter.InsertTextBefore(startPosition, " != (");
							TheRewriter.InsertTextBefore(startPosition, iter->variable);
							TheRewriter.InsertTextBefore(startPosition, "__VERIFIER_assert(");
							--localOffset;
						}

						iter = entry.writes.rbegin();
						endIter = entry.writes.rend();
						localOffset = entry.writes.size() - 1;
						for (; iter != endIter; ++iter) {
							TheRewriter.InsertTextBefore(startPosition, "\n");
							TheRewriter.InsertTextBefore(startPosition, std::to_string(writeCounter + localOffset));
							TheRewriter.InsertTextBefore(startPosition, ") 0x0); // write");
							if (isa<ArrayType>(iter->type.getTypePtr())) {
								const ArrayType* arrayType = cast<ArrayType>(iter->type.getTypePtr());
								std::string assertionString = arrayType->getElementType().getAsString() + "*";
								TheRewriter.InsertTextBefore(startPosition, assertionString);
							}
							else {

								std::string assertionString = iter->type.getAsString();
								TheRewriter.InsertTextBefore(startPosition, assertionString);
							}
							TheRewriter.InsertTextBefore(startPosition, " != (");
							TheRewriter.InsertTextBefore(startPosition, iter->variable);
							TheRewriter.InsertTextBefore(startPosition, "__VERIFIER_assert(");
							--localOffset;
						}

						TheRewriter.InsertTextBefore(startPosition, ";\n");
						readCounter += entry.reads.size();
						writeCounter += entry.writes.size();
					}
					else {
						std::cerr << "TODO add assert before\n";
						readCounter += entry.reads.size();
						writeCounter += entry.writes.size();
					}
					break;
				}
				else {
					std::cerr << "Instrumentation INITIALIZATION not implemented for multiple declarations in a single statement" << std::endl;
					declStmt->dumpColor();
					readCounter += entry.reads.size();
					writeCounter += entry.writes.size();
					break;
				}

			} break;
			case PositionType::SINGLE_INITIALIZATION: {
				std::cerr << "Instrumentation SINGLE_INITIALIZATION not implemented" << std::endl;
				readCounter += entry.reads.size();
				writeCounter += entry.writes.size();
			} break;
			}
		}


	}
	else {
		parseInstrumentationFromJSON(InstrumentationJSON, entries, assertsToEnter, numReads);


		for (auto&& assertion : assertsToEnter) {
			auto& statement = entries.at(std::get<0>(assertion));
			switch (statement.type)
			{
			case PositionType::NORMAL: {
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), ");");
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), std::get<1>(assertion));
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), "__VERIFIER_assert(");
			} break;
			case PositionType::CONDITION_INIT_OR_STEP: {
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), "),");
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), std::get<1>(assertion));
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), "__VERIFIER_assert(");
			} break;
			case PositionType::SINGLE_STATEMENT: {
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), ");");
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), std::get<1>(assertion));
				TheRewriter.InsertTextBefore(statement.statement->getLocStart(), "{__VERIFIER_assert(");
				SourceLocation SemiLoc = findLocationAfterSemi(statement.statement->getLocEnd(), TheCompInst.getASTContext(), false);
				TheRewriter.InsertTextBefore(SemiLoc, "}");
			} break;
			case PositionType::INITIALIZATION: {
				const DeclStmt* declStmt = cast<DeclStmt>(statement.statement);
				if (declStmt->isSingleDecl()) {
					const VarDecl* varDecl = cast<VarDecl>(declStmt->getSingleDecl());
					if (varDecl->getInitStyle() == clang::VarDecl::InitializationStyle::CInit) {
						auto init = varDecl->getInit();
						auto startPosition = varDecl->getLocStart();
						while (*SourceMgr.getCharacterData(startPosition) != '=') {
							startPosition = startPosition.getLocWithOffset(1);
						}
						TheRewriter.InsertTextBefore(startPosition, varDecl->getQualifiedNameAsString());
						TheRewriter.InsertTextBefore(startPosition, ");");
						TheRewriter.InsertTextBefore(startPosition, std::get<1>(assertion));
						TheRewriter.InsertTextBefore(startPosition, ";__VERIFIER_assert(");
					}
					else {
						std::cerr << "TODO add assert before\n";
					}
					break;
				}
				else {
					std::cerr << "Instrumentation INITIALIZATION not implemented for multiple declarations in a single statement" << std::endl;
					declStmt->dumpColor();
					break;
				}


				std::cerr << "Instrumentation INITIALIZATION not implemented" << std::endl;

			} break;
			case PositionType::SINGLE_INITIALIZATION: {
				std::cerr << "Instrumentation SINGLE_INITIALIZATION not implemented" << std::endl;
			} break;
			}

		}
	}





	// At this point the rewriter's buffer should be full with the rewritten
	// file contents.
	const RewriteBuffer *RewriteBuf = TheRewriter.getRewriteBufferFor(SourceMgr.getMainFileID());
	std::ofstream o(InstrumentedFileName);
	o << std::string(RewriteBuf->begin(), RewriteBuf->end()) << std::endl;
	o.close();


	return;
}


constexpr char hexmap[] = { '0', '1', '2', '3', '4', '5', '6', '7',
'8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

std::string hexStr(unsigned char *data, int len)
{
	std::string s(len * 2, ' ');
	for (int i = 0; i < len; ++i) {
		s[2 * i] = hexmap[(data[i] & 0xF0) >> 4];
		s[2 * i + 1] = hexmap[data[i] & 0x0F];
	}
	return s;
}

int main(int argc, char *argv[]) {
	std::string JSONOutputFile="";
	std::string SrcFileName = "";
	std::string InstrumentedFileName = "";
	std::string InstrumentationJSON = "";
	auto cli
		= Opt(JSONOutputFile, "JSONOutputFile")
		["-j"]["--json"]
		("Name for the JSON file containing the reads and writes.")
		| Opt(InstrumentedFileName, "InstrumentedFileName")
		["-i"]["--instrumented"]
		("File name for the instrumented version.")
		| Opt(InstrumentationJSON, "InstrumentationJSON")
		["-a"]["--access"]
		("JSON for which read/write should be manipulated and the corresponding value.")
		| Arg(SrcFileName, "source")
		("which file to read/instrument").required();
	auto result = cli.parse(Args(argc, argv));
	if (!result) {
		std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
		std::cerr << cli << std::endl;
		exit(1);
	}
	if (SrcFileName.empty()) {
		std::cerr << "Error in command line: No source file specified" << std::endl;
		std::cerr << cli << std::endl;
		exit(1);
	}

	if (InstrumentedFileName.empty()) {
		std::cerr << "Error in command line: InstrumentedFileName not specified" << std::endl;
		std::cerr << cli << std::endl;
		exit(1);
	}
	instrument(SrcFileName, JSONOutputFile, InstrumentationJSON, InstrumentedFileName);

	return 0;
}

void parseInstrumentationFromJSON(std::string &InstrumentationJSON, std::vector<Entry> &entries, std::vector<std::tuple<int, std::string>> &assertsToEnter, int &numReads)
{

	std::ifstream i(InstrumentationJSON);
	if(!i.is_open()) {
		std::cerr << "could not open the assertion file "<< InstrumentationJSON <<"\n";
		exit(1);
	}
	json instrumentationJSON;
	i >> instrumentationJSON;

	if (instrumentationJSON.is_object()) {
		auto isRead = instrumentationJSON.find("isRead");
		bool read = false;
		if (isRead != instrumentationJSON.end() && (*isRead).is_boolean()) {
			read = *isRead;
		}
		else {
			std::cerr << "isRead should be set - assuming false\n";
		}
		auto Number = instrumentationJSON.find("number");
		if (Number != instrumentationJSON.end() && Number->is_number_integer()) {
			int number = *Number;
			auto Value = instrumentationJSON.find("value");
			if (Value != instrumentationJSON.end() && Value->is_string()) {
				std::string valueHex = *Value;


				if (read) {
					int counter = 0;
					for (auto&& entry : entries) {
						int size = entry.reads.size();
						if (number >= size) {
							number -= size;
						}
						else {
							auto type = entry.reads.at(number).type.getUnqualifiedType();
							if (isa<ArrayType>(type.getTypePtr())) {
								const ArrayType* arrayType = cast<ArrayType>(type.getTypePtr());
								std::string assertionString = entry.reads.at(number).variable + " != (" + arrayType->getElementType().getAsString() + "*) 0x" + valueHex;
								std::tuple<int, std::string> assertion(counter, assertionString);
								assertsToEnter.push_back(assertion);
								return;
							}
							std::string assertionString = entry.reads.at(number).variable + " != ("+ entry.reads.at(number).type.getAsString()+") 0x" + valueHex;
							std::tuple<int, std::string> assertion(counter, assertionString);
							assertsToEnter.push_back(assertion);
							return;
						}
						++counter;
					}
					std::cerr << "read " << instrumentationJSON["number"] << " could not be found" << std::endl;
				}
				else {
					int counter = 0;
					for (auto&& entry : entries) {
						int size = entry.writes.size();
						if (number >= size) {
							number -= size;
						}
						else {
							auto type = entry.writes.at(number).type.getUnqualifiedType();
							if (isa<ArrayType>(type.getTypePtr())) {
								const ArrayType* arrayType = cast<ArrayType>(type.getTypePtr());
								std::string assertionString = entry.writes.at(number).variable + " != (" + arrayType->getElementType().getAsString() + " *) 0x" + valueHex;
								std::tuple<int, std::string> assertion(counter, assertionString);
								assertsToEnter.push_back(assertion);
								return;
							}
							std::string assertionString = entry.writes.at(number).variable + " != (" + entry.writes.at(number).type.getAsString() + ") 0x" + valueHex;
							std::tuple<int, std::string> assertion(counter, assertionString);
							assertsToEnter.push_back(assertion);
							return;
						}
						++counter;
					}
					std::cerr << "write " << instrumentationJSON["number"]<< " could not be found" << std::endl;
				}

			}
			else {
				std::cerr << "value must be a string containing a hex value\n";
			}

		}
		else {
			std::cerr << "number must be an integer\n";
		}

	}
	else {
		std::cerr << "input must be JSON object\n";
	}
	exit(1);
}

