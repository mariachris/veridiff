
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <tuple>
#include <fstream>
#include <random>

#include "clara.hpp"
#include "json.hpp"

// for convenience
using json = nlohmann::json;
using namespace clara;





constexpr char hexmap[] = { '0', '1', '2', '3', '4', '5', '6', '7',
'8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

std::string hexStr(unsigned char *data, int len)
{
	std::string s(len * 2, ' ');
	for (int i = 0; i < len; ++i) {
		s[2 * i] = hexmap[(data[i] & 0xF0) >> 4];
		s[2 * i + 1] = hexmap[data[i] & 0x0F];
	}
	return s;
}


std::tuple<int, int> getMaxIndexFromFile(int currentMax, std::string JSONOutputFileFromTool) {
	std::ifstream i(JSONOutputFileFromTool);
	if (!i.is_open()) {
		std::cerr << "could not open the tool output file " << JSONOutputFileFromTool << "\n";
		return std::make_tuple(currentMax, currentMax);
	}
	json instrumentationJSON;
	i >> instrumentationJSON;

	if (instrumentationJSON.is_object()) {
		auto NumberOfReads = instrumentationJSON.find("numberOfReads");
		if (NumberOfReads == instrumentationJSON.end() || !NumberOfReads->is_number_integer()) {
			std::cerr << "the specified file does not contain a JSON object with key \"numberOfReads\"\n";
			return std::make_tuple(currentMax, currentMax);
		}
		auto NumberOfWrites = instrumentationJSON.find("numberOfWrites");
		if (NumberOfWrites == instrumentationJSON.end() || !NumberOfWrites->is_number_integer()) {
			std::cerr << "the specified file does not contain a JSON object with key \"numberOfWrites\"\n";
			return std::make_tuple(currentMax, currentMax);
		}
		int numberOfReads = *NumberOfReads;
		int numberOfWrites = *NumberOfWrites;
		return std::make_tuple(numberOfReads, numberOfWrites);
	}
	else {
		std::cerr << "the specified file does not contain a JSON object\n";
	}


	return std::make_tuple(currentMax, currentMax);
}




int main(int argc, char *argv[]) {
	std::string JSONOutputFileFromTool="";
	std::string assertionFileName = "";
	int maxIndex = 1000;
	auto cli
		= Opt(maxIndex, "maxIndex")
		["-i"]["--instrumented"]
		("File name for the instrumented version.")
		| Opt(JSONOutputFileFromTool, "JSONOutputFileFromTool")
		["-j"]["--json"]
		("Name for the JSON file containing the reads and writes.")
		| Arg(assertionFileName, "assertionFileName")
		("where to write the assertion file").required();
	auto result = cli.parse(Args(argc, argv));
	if (!result) {
		std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
		std::cerr << cli << std::endl;
		exit(1);
	}
	if (assertionFileName.empty()) {
		std::cerr << "Error in command line: No output file specified" << std::endl;
		std::cerr << cli << std::endl;
		exit(1);
	}
	if (maxIndex < 1) {
		std::cerr << "Error in command line: maxIndex is less than 1" << std::endl;
		maxIndex = 1000;
	}


	int reads = maxIndex;
	int writes = maxIndex;
	if (!JSONOutputFileFromTool.empty()) {
		std::tie(reads, writes) = getMaxIndexFromFile(maxIndex, JSONOutputFileFromTool);
	}


		json instrumentationJSON = json::object();

		int access = reads + writes;

		std::random_device rd;
		std::mt19937_64 random_generator(rd());
		std::uniform_int_distribution<> accessIndexGenerator(0, access - 1);

		int index = accessIndexGenerator(random_generator);
		std::uniform_int_distribution<uint64_t> valueGenerator{};
		uint64_t value = valueGenerator(random_generator);
		
		if (index < reads) {

			instrumentationJSON["value"] = hexStr((unsigned char *)(&value), 8);
				instrumentationJSON["number"] = index;
				instrumentationJSON["isRead"] = true;
		}else{
			instrumentationJSON["value"] = hexStr((unsigned char *)(&value), 8);
				instrumentationJSON["number"] = index - reads;
				instrumentationJSON["isRead"] = false;
		}


		{
			std::ofstream o(assertionFileName);
			o << std::setw(2) << instrumentationJSON << std::endl;
			o.close();
		}
	


	return 0;
}

