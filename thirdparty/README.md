# Building Clang and LLVM

You need Python 2

## Windows 

### Build LLVM and Clang
- create a subfolder vs
- in that directory execute ``cmake -G "Visual Studio 15 2017 Win64" -Thost=x64 ..``
- open the generated solution and build the INSTALL project

### Build the tool
- create a subfolder vs
- in that directory execute ``cmake -G "Visual Studio 15 2017 Win64" -Thost=x64 ..``
- open the generated solution and build the veridiff project