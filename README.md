# VeriDiff

# Limitation

- we only track some writes 
- we ignore declaration of global variables (no splitting up declaration and initialization)

# Example
veridiff.exe in.c -i instrumented.c -j statements.json -a assert.json